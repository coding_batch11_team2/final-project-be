CREATE DATABASE Dlanguage;

USE Dlanguage;

# Create the User table
CREATE TABLE USERS (
    UserID INT PRIMARY KEY AUTO_INCREMENT,
    Full_Name VARCHAR(100) NOT NULL,
    Email VARCHAR(255) UNIQUE,
    PASSWORD VARCHAR(255),
    ROLE INT DEFAULT 0
);

# Create the Category table
CREATE TABLE Category (
    CategoryID INT PRIMARY KEY AUTO_INCREMENT,
    CategoryName VARCHAR(255),
    DESCRIPTION TEXT,
    BannerImage VARCHAR(255),
    FlagImage VARCHAR(255)
);

# Create the Schedule table
CREATE TABLE SCHEDULE (
    ScheduleID INT PRIMARY KEY AUTO_INCREMENT,
    ScheduleDate DATETIME
);

# Create the Course table
CREATE TABLE Course (
    CourseID INT PRIMARY KEY AUTO_INCREMENT,
    CourseName VARCHAR(255),
    DESCRIPTION TEXT,
    ImageURL VARCHAR(255),
    Price DECIMAL(10, 2),
    FK_CategoryID INT,
    FOREIGN KEY (FK_CategoryID) REFERENCES Category(CategoryID)
);

# Create the Checkout table
CREATE TABLE Checkout (
    CheckoutID INT PRIMARY KEY AUTO_INCREMENT,
    Checkout_Date DATETIME,
    Method_Name VARCHAR(25),
    FK_UserID INT,
    FOREIGN KEY (FK_UserID) REFERENCES USERS(UserID)
);

# Create the CheckoutDetail table
CREATE TABLE CheckoutDetail (
    CheckoutDetailID INT PRIMARY KEY AUTO_INCREMENT,
    FK_CheckoutID INT,
    FK_CourseID INT,
    FK_ScheduleID INT,
    FOREIGN KEY (FK_ScheduleID) REFERENCES SCHEDULE(ScheduleID),
    FOREIGN KEY (FK_CheckoutID) REFERENCES Checkout(CheckoutID),
    FOREIGN KEY (FK_CourseID) REFERENCES Course(CourseID)
);

# Create the Invoice table
CREATE TABLE Invoice (
    InvoiceID INT PRIMARY KEY AUTO_INCREMENT,
    Invoice_Number VARCHAR(25) UNIQUE,
    PurchaseDate DATETIME,
    TotalCourses INT,
    TotalPrice DECIMAL(10, 2),
    FK_CheckoutID INT,
    FOREIGN KEY (FK_CheckoutID) REFERENCES Checkout(CheckoutID)
);
