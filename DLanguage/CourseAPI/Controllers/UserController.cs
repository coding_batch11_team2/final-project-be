using CourseAPI.Models;
using CourseAPI.Dto;
using CourseAPI.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CourseAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private UserRepository userRepository;
        public UserController(UserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        [HttpGet]
        public IActionResult getAlluser() 
        { 
            List<User> Users = userRepository.getAllUser();
            return Ok(Users);
        }

        [HttpPost]
        public IActionResult createUser([FromBody] CreateUserDto data)
        {
            userRepository.Create(data.Name, data.Email, data.Password);
            return Ok("Added New Category Course");
        }

       

    }
} 