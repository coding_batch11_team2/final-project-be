﻿using CourseAPI.Dto;
using CourseAPI.Models;
using CourseAPI.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CourseAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CourseController : ControllerBase
    {
        private CourseRepository courseRepository;
        //dependency injection
        public CourseController(CourseRepository courseRepository) {
            this.courseRepository = courseRepository;
        }

        //Get All Courses Data
        [HttpGet]
        public IActionResult getAllCourses()
        {
            List<Course> courses = courseRepository.getAll();
            return Ok(courses);
        }
        //Get Active Courses
        [HttpGet("Active")]
        public IActionResult getctiveCourses()
        {
            List<Course> courses = courseRepository.getActiveCourse();
            return Ok(courses);
        }
        //Create new Course
        [HttpPost]
        public IActionResult createCourse([FromBody] CreateCourseDto data)
        {
            courseRepository.Create(data.Title, data.Description, data.Price, data.Is_Active, data.fk_id_category);
            return Ok("Added New Course");
        }

        //Update Course
        [HttpPut("{id}")]
        public IActionResult updateCourse(int id, [FromBody] CreateCourseDto data)
        {
            courseRepository.Update(id, data.Title, data.Description, data.Price, data.Is_Active, data.fk_id_category);
            return Ok("Successfully Update Course With Id : "+id);
        }

        //Update Course Status (Active or In Active)
        [HttpPatch("{id}")]
        public IActionResult updateCourseStatus(int id, [FromBody] int Is_Active)
        {
            courseRepository.UpdateStatus(id, Is_Active);
            return Ok("Course Status Successfully Updated!");
        }
    }
}
