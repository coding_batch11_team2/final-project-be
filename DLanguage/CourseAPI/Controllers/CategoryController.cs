﻿using CourseAPI.Dto;
using CourseAPI.Models;
using CourseAPI.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CourseAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private CategoryRepository categoryRepository;
        public CategoryController(CategoryRepository categoryRepository)
        {
            this.categoryRepository = categoryRepository;
        }
        [HttpGet]
        public IActionResult getAllCategory()
        {
            List<Category> categories = categoryRepository.getAll();
            //List<Course> courses = courseRepository.getAll();
            return Ok(categories);
        }

        [HttpPost]
        public IActionResult createCategory([FromBody] CreateCategoryDto data)
        {
            categoryRepository.Create(data.Title, data.Description, data.Is_Active);
            return Ok("Added New Category Course");
        }

        [HttpPut("{id}")]
        public IActionResult updateCategory(int id, [FromBody] CreateCategoryDto data)
        {
            categoryRepository.Update(id, data.Title, data.Description, data.Is_Active);
            return Ok("Successfully Update Course With Id : " + id);
        }

        [HttpPatch("{id}")]
        public IActionResult updateCategoryStatus(int id, [FromBody] int Is_Active)
        {
            categoryRepository.UpdateStatus(id, Is_Active);
            return Ok("Course Status Successfully Updated!");
        }
    }
}
