﻿namespace CourseAPI.Dto
{
    public class CreateCourseDto
    {
        public string Title { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public int Price { get; set; }
        public int Is_Active { get; set; }
        public int fk_id_category { get; set; }
    }
}
