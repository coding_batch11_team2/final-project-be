﻿namespace CourseAPI.Dto
{
    public class CreateCategoryDto
    {
        public string Title { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public int Is_Active { get; set; }
    }
}
