﻿namespace CourseAPI.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string CategoryName { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string BannerImage { get; set; } = string.Empty;
        public string FlagImage { get; set; } = string.Empty;
        public bool Is_Active { get; set; }
    }
}
