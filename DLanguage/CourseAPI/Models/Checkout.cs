namespace CourseAPI.Models
{
    public class Checkout
    {
        public int Id { get; set; }
        public string Checkout_Date { get; set; } = string.Empty;
        public string Method_Name { get; set; } = string.Empty;
        public int FK_UserID { get; set; }
    }
}
