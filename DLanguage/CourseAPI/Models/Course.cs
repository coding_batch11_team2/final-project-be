﻿namespace CourseAPI.Models
{
    public class Course
    {
        public int Id { get; set; }
        public string CourseName { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string ImageUrl { get; set; } = string.Empty;
        public bool Is_Active { get; set; }
        public int fk_id_category { get; set; }
    }
}
