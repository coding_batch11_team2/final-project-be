﻿using CourseAPI.Models;
using MySql.Data.MySqlClient;

namespace CourseAPI.Repositories
{
    public class CartRepository
    {
        private string connStr = string.Empty;
        public CartRepository(IConfiguration configuration)
        {
            connStr = configuration.GetConnectionString("Default");
        }
        //"Default": "server=localhost;user=root;database=dlanguage;port=3306;password="

        public List<Cart> getAllCart()
        {
            List<Cart> carts = new List<Cart>();

            //connect to database
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                Console.WriteLine("Connecting to MySQL...");
                conn.Open();
                // Perform database operations
                MySqlCommand cmd = new MySqlCommand("SELECT cs.CoursID, cs.CourseName, cs.ImageURL, cs.Price, s.ScheduleDate, cg.CategoryName " +
                "FROM Cart c" +
                "INNER JOIN Users u ON c.FK_UserID = u.UserID " +
                "INNER JOIN C_Schedule s ON c.FK_ScheduleID = s.ScheduleID " +
                "INNER JOIN Course cs ON s.FK_CourseID = cs.CourseID " +
                "INNER JOIN Category AS cg ON cs.FK_CategoryID = cg.CategoryID", conn);

                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    int id = reader.GetInt32("CartID");
                    string fk_scheduleID = reader.GetString("FK_ScheduleID");
                    string fk_userID = reader.GetString("FK_UserID");
                   

                    carts.Add(new Cart
                    {
                        Id = id,
                        FK_Schedule = fk_scheduleID,
                        FK_UserID = fk_userID
                    });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            conn.Close();
            Console.WriteLine("Done.");
            return carts;
        }



        public void Create(string fk_ScheduleID, string fk_userID)
        {
            //string connStr = "server=localhost;user=root;database=dlanguage;port=3306;password=";

            //connect to database
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                Console.WriteLine("Connecting to MySQL...");
                conn.Open();
                // Perform database operations
                MySqlCommand cmd = new MySqlCommand("INSERT INTO Cart(FK_ScheduleID, FK_UserID) VALUES (@FK_ScheduleID,FK_UserID)", conn);
                cmd.Parameters.AddWithValue("@FK_ScheduleID", fk_ScheduleID);
                cmd.Parameters.AddWithValue("@FK_UserID", fk_userID);


                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            conn.Close();
        }

        public void Update(int id, string name, string email, string password)
        {
            //string connStr = "server=localhost;user=root;database=dlanguage;port=3306;password=";

            //connect to database
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                Console.WriteLine("Connecting to MySQL...");//buat liat jalan apa tidak
                conn.Open();
                // Perform database operations Name itu dari databae, @Name variabel tampungan, name parameter yang di atas.
                MySqlCommand cmd = new MySqlCommand("UPDATE Users SET Name = @Name, Email = @Email, Password = @Password WHERE id = @Id", conn);
                cmd.Parameters.AddWithValue("@Name", name);
                cmd.Parameters.AddWithValue("@Email", email);
                cmd.Parameters.AddWithValue("@Id", id);


                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            conn.Close();
        }
        public void Delete(int id)
        {
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                Console.WriteLine("Connecting to MySQL..."); // Indicates a database connection attempt
                conn.Open();

                MySqlCommand cmd = new MySqlCommand("DELETE FROM Cart WHERE CartID = @Id", conn);
                cmd.Parameters.AddWithValue("@Id", id);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            conn.Close();
        }

    }
}
