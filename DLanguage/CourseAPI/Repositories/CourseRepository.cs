﻿using CourseAPI.Models;
using MySql.Data.MySqlClient;

namespace CourseAPI.Repositories
{
    public class CourseRepository
    {  
        private string connStr = string.Empty;
        public CourseRepository(IConfiguration configuration){
            connStr = configuration.GetConnectionString("Default");
        }
        public List<Course> getAll()
        {
            List<Course> courses = new List<Course>();
            //string connStr = "server=localhost;user=root;database=dlanguage;port=3306;password=";

            //connect to database
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                Console.WriteLine("Connecting to MySQL...");
                conn.Open();
                // Perform database operations
                MySqlCommand cmd = new MySqlCommand("SELECT id, title, description, price, is_active, fk_id_category FROM course", conn);
                MySqlDataReader reader= cmd.ExecuteReader();
                while (reader.Read())
                {
                    int id = reader.GetInt32("id");
                    string title = reader.GetString("title");
                    string decription = reader.GetString("description");
                    int price = reader.GetInt32("price");
                    int is_active = reader.GetInt32("is_active");
                    int fk_id_category = reader.GetInt32("fk_id_category");
                    courses.Add(new Course
                    {
                        Id = id,
                        Title = title,
                        Description = decription,
                        Price = price,
                        Is_Active = is_active,
                        fk_id_category = fk_id_category
                    });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            conn.Close();
            Console.WriteLine("Done.");
            return courses;
        }

        public List<Course> getActiveCourse()
        {
            List<Course> courses = new List<Course>();
            //string connStr = "server=localhost;user=root;database=dlanguage;port=3306;password=";

            //connect to database
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                Console.WriteLine("Connecting to MySQL...");
                conn.Open();
                // Perform database operations
                MySqlCommand cmd = new MySqlCommand("SELECT id, title, description, price, is_active, fk_id_category FROM course WHERE is_active = 1", conn);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    int id = reader.GetInt32("id");
                    string title = reader.GetString("title");
                    string decription = reader.GetString("description");
                    int price = reader.GetInt32("price");
                    int is_active = reader.GetInt32("is_active");
                    int fk_id_category = reader.GetInt32("fk_id_category");
                    courses.Add(new Course
                    {
                        Id = id,
                        Title = title,
                        Description = decription,
                        Price = price,
                        Is_Active = is_active,
                        fk_id_category = fk_id_category
                    });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            conn.Close();
            Console.WriteLine("Done.");
            return courses;
        }

        public void Create(string title, string description, int price, int is_act, int fk_id_cat)
        {
            //string connStr = "server=localhost;user=root;database=dlanguage;port=3306;password=";

            //connect to database
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                Console.WriteLine("Connecting to MySQL...");
                conn.Open();
                // Perform database operations
                MySqlCommand cmd = new MySqlCommand("INSERT INTO course(title, description, price, is_active, fk_id_category) VALUES (@Title, @Description, @Price, @Is_Act, @Fk_Id_Cat)", conn);
                cmd.Parameters.AddWithValue("@Title", title);
                cmd.Parameters.AddWithValue("@Description", description);
                cmd.Parameters.AddWithValue("@Price", price);
                cmd.Parameters.AddWithValue("@Is_Act", is_act);
                cmd.Parameters.AddWithValue("@Fk_Id_Cat", fk_id_cat);

                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            conn.Close();
        }
        public void Update(int id, string title, string description, int price, int is_act, int fk_id_cat)
        {
            //string connStr = "server=localhost;user=root;database=dlanguage;port=3306;password=";

            //connect to database
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                Console.WriteLine("Connecting to MySQL...");
                conn.Open();
                // Perform database operations
                MySqlCommand cmd = new MySqlCommand("UPDATE course SET title = @Title, description = @Description, price = @Price, is_active = @Is_Act, fk_id_category = @Fk_Id_Cat WHERE id = @Id", conn);
                cmd.Parameters.AddWithValue("@Title", title);
                cmd.Parameters.AddWithValue("@Description", description);
                cmd.Parameters.AddWithValue("@Price", price);
                cmd.Parameters.AddWithValue("@Is_Act", is_act);
                cmd.Parameters.AddWithValue("@Fk_Id_Cat", fk_id_cat);
                cmd.Parameters.AddWithValue("@Id", id);

                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            conn.Close();
        }
        public void UpdateStatus(int id, int is_act)
        {
            //string connStr = "";

            //connect to database
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                Console.WriteLine("Connecting to MySQL...");
                conn.Open();
                // Perform database operations
                MySqlCommand cmd = new MySqlCommand("UPDATE course SET is_active = @Is_Act WHERE id = @Id", conn);
                cmd.Parameters.AddWithValue("@Is_Act", is_act);
                cmd.Parameters.AddWithValue("@Id", id);

                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            conn.Close();
        }
    }
}
