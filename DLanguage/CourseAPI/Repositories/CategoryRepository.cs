﻿using CourseAPI.Models;
using MySql.Data.MySqlClient;

namespace CourseAPI.Repositories
{
    public class CategoryRepository
    {
        private string connStr = string.Empty;
        public CategoryRepository(IConfiguration configuration)
        {
            connStr = configuration.GetConnectionString("Default");
        }

        public List<Category> getAll()
        {
            List<Category> categories = new List<Category>();
            //string connStr = "server=localhost;user=root;database=dlanguage;port=3306;password=";

            //connect to database
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                Console.WriteLine("Connecting to MySQL...");
                conn.Open();
                // Perform database operations
                MySqlCommand cmd = new MySqlCommand("SELECT id, title, description, is_act FROM category_course", conn);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    int id = reader.GetInt32("id");
                    string title = reader.GetString("title");
                    string decription = reader.GetString("description");
                    int is_act = reader.GetInt32("is_act");
                    categories.Add(new Category
                    {
                        Id = id,
                        Title = title,
                        Description = decription,
                        Is_Active = is_act
                    });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            conn.Close();
            Console.WriteLine("Done.");
            return categories;
        }

        public void Create(string title, string description, int is_act)
        {
            //string connStr = "server=localhost;user=root;database=dlanguage;port=3306;password=";

            //connect to database
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                Console.WriteLine("Connecting to MySQL...");
                conn.Open();
                // Perform database operations
                MySqlCommand cmd = new MySqlCommand("INSERT INTO category_course(title, description, is_act) VALUES (@Title, @Description, @Is_Act)", conn);
                cmd.Parameters.AddWithValue("@Title", title);
                cmd.Parameters.AddWithValue("@Description", description);
                cmd.Parameters.AddWithValue("@Is_Act", is_act);

                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            conn.Close();
        }

        public void Update(int id, string title, string description,int is_act)
        {
            //string connStr = "server=localhost;user=root;database=dlanguage;port=3306;password=";

            //connect to database
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                Console.WriteLine("Connecting to MySQL...");
                conn.Open();
                // Perform database operations
                MySqlCommand cmd = new MySqlCommand("UPDATE category_course SET title = @Title, description = @Description, is_act = @Is_Act WHERE id = @Id", conn);
                cmd.Parameters.AddWithValue("@Title", title);
                cmd.Parameters.AddWithValue("@Description", description);
                cmd.Parameters.AddWithValue("@Is_Act", is_act);
                cmd.Parameters.AddWithValue("@Id", id);

                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            conn.Close();
        }

        public void UpdateStatus(int id, int is_act)
        {
            //string connStr = "";

            //connect to database
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                Console.WriteLine("Connecting to MySQL...");
                conn.Open();
                // Perform database operations
                MySqlCommand cmd = new MySqlCommand("UPDATE category_course SET is_act = @Is_Act WHERE id = @Id", conn);
                cmd.Parameters.AddWithValue("@Is_Act", is_act);
                cmd.Parameters.AddWithValue("@Id", id);

                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            conn.Close();
        }

    }
}
