﻿using CourseAPI.Models;
using MySql.Data.MySqlClient;

namespace CourseAPI.Repositories
{
    public class CheckoutRepository
    {
        private string connStr = string.Empty;
        public CheckoutRepository (IConfiguration configuration)
        {
            connStr = configuration.GetConnectionString("Default");
        }
        //"Default": "server=localhost;user=root;database=dlanguage;port=3306;password="

        public List<User> getAllUser()
        {
            List<User> users = new List<User>();

            //connect to database
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                Console.WriteLine("Connecting to MySQL...");
                conn.Open();
                // Perform database operations
                MySqlCommand cmd = new MySqlCommand("SELECT Userid, Name, Email, Password, Role FROM users", conn);
                MySqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    int id = reader.GetInt32("Userid");
                    string name = reader.GetString("Name");
                    string email = reader.GetString("Email");
                    string password = reader.GetString("Password");
                    int role = reader.GetInt32("Role");

                    users.Add(new User
                    {
                        Id = id,
                        Name = name,
                        Email = email,
                        Password = password,
                        Role = role
                    });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            conn.Close();
            Console.WriteLine("Done.");
            return users;
        }



        public void Create(string name, string email, string password)
        {
            //string connStr = "server=localhost;user=root;database=dlanguage;port=3306;password=";

            //connect to database
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                Console.WriteLine("Connecting to MySQL...");
                conn.Open();
                // Perform database operations
                MySqlCommand cmd = new MySqlCommand("INSERT INTO users(Name, Email, Password) VALUES (@Name, @Email, @Password)", conn);
                cmd.Parameters.AddWithValue("@Name", name);
                cmd.Parameters.AddWithValue("@Email", email);
                cmd.Parameters.AddWithValue("@Password", password);


                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            conn.Close();
        }

        public void Update(int id, string name, string email, string password)
        {
            //string connStr = "server=localhost;user=root;database=dlanguage;port=3306;password=";

            //connect to database
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                Console.WriteLine("Connecting to MySQL...");//buat liat jalan apa tidak
                conn.Open();
                // Perform database operations Name itu dari databae, @Name variabel tampungan, name parameter yang di atas.
                MySqlCommand cmd = new MySqlCommand("UPDATE Users SET Name = @Name, Email = @Email, Password = @Password WHERE id = @Id", conn);
                cmd.Parameters.AddWithValue("@Name", name);
                cmd.Parameters.AddWithValue("@Email", email);
                cmd.Parameters.AddWithValue("@Password", password);
                cmd.Parameters.AddWithValue("@Id", id);


                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            conn.Close();
        }
    }
}
